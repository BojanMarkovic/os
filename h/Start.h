#ifndef _START_H_
#define _START_H_

#include "LIST.h"

class List;

class Start {
public:
	static void first();
	static void last();
	static volatile int dispatched;
	static volatile int sleepFlag;
	static Thread* starting;
	static IdleThread* idle;
	static volatile PCB* running;
	static volatile List *sleepList;
private:
	friend class Thread;
};

#endif
