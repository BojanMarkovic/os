#ifndef _PCB_H_ 
#define _PCB_H_

#include "LIST.h"
#include "thread.h" 
#include <dos.h>
#include "schedule.h"
#include "TimerK.h"
#include "Start.h"

class Thread;
class List;
class PCB {
public:
	PCB(Thread* myThread, StackSize stackSize, Time timeSlice);
	~PCB();
	void startPCB();
	void waitToCompletePCB();
	void createStack();
	void clearWaitingList();
	static void dispatch();

	Time timeSlice;
	StackSize stackSize;
	Thread* myThread;
	unsigned int sp, ss;
	unsigned long* stack;
	enum ThreadStatus {
		READY, SLEEPING, BLOCKED, OVER, NEW
	};
	enum ThreadStatus status;

	Time leftTime;

	List* waitList;

	static void wrapper();
	static void clearSleepList();
	static void sleepPCB(Time timeToSleep);
};

#endif
