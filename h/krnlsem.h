#ifndef _KRNLSEM_H_ 
#define _KRNLSEM_H_

#include "LIST.h"
#include "schedule.h"

class KernelSem {
public:
	friend class Semaphore;

	int wait(int toBlock);
	void signal();

	int val() const{
		return value;
	}
	KernelSem(int init = 1);
private:
	int value;
	List waitQueue;
};

#endif
