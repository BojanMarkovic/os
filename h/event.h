#ifndef _event_h_
#define _event_h_

typedef unsigned char IVTNo;
class KernelEv;

class Event {
public:
	Event(IVTNo ivtNo);
	~Event();

	void wait();

protected:

	friend class KernelEv;
	void signal(); // can call KernelEv

private:
	KernelEv* myImpl;
};

#define PREPAREENTRY(numIVT,oldINT)\
	void interrupt int##numIVT();\
	IVTEntry entry##numIVT(##numIVT,int##numIVT);\
    void interrupt int##numIVT(){\
            IVTEntry::entries[##numIVT]->signalEvent();\
            if (oldINT) IVTEntry::entries[##numIVT]->oldRoutine();}

#endif
