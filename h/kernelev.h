#ifndef _KERNELEV_H_ 
#define _KERNELEV_H_

#include "event.h"
#include "LIST.h"
#include "schedule.h"
#include "lock.h"
#include "IVTEntry.h"

class KernelEv {
public:
	void wait();
	void signal();
	KernelEv(IVTNo);
	~KernelEv();

private:
	IVTNo ivtEntry;
	List *waitList;
	PCB *owner;
	int value;
};

#endif
