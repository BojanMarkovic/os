#ifndef _TimerK_H_
#define _TimerK_H_

#include "pcb.h" 
#include "idletred.h"
#include "event.h"
#include "lock.h"
#include "schedule.h"
#include <dos.h>

typedef void (interrupt *InterruptRoutine)();

class TimerK {
public:
	static InterruptRoutine replaceRoutine(IVTNo intN,
			InterruptRoutine newRoutine);
	static void interrupt TIMER();
	static void interrupt (*oldRoutine)();
private:
	friend class Thread;
	friend class PCB;
};

#endif
