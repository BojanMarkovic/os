#ifndef _IDLETRED_H_ 
#define _IDLETRED_H_

#include "thread.h" 

class Thread;
class IdleThread: public Thread {
public:
	IdleThread() : Thread(128, 1) {}
	void run() {
		while (1);
	}
};

#endif
