#ifndef _LOCK_H_
#define _LOCK_H_

#define lock asm{\
	pushf;\
	cli;\
}

#define unlock asm popf

#endif
