#ifndef _QUEUE_H_
#define _QUEUE_H_

#include "pcb.h"
class PCB;

class List {
public:
	void add(PCB* dat);
	PCB* get();

	List();
	~List();

	void setIterHead() {
		iter = head;
	}
	void goNextIter();
	PCB* getIter() {
		if (iter == 0)
			return 0;
		return iter->data;
	}
	int size() const {
		return br;
	}
	int remove(PCB* p);

private:
	struct Elem {
		PCB* data;
		Elem* next;
		Elem(PCB* d, Elem* n = 0) :
				data(d), next(n) {
		}
	};
	Elem *head, *last, *iter;
	int br, flag;
};

#endif
