#ifndef _IVTENTRY_H_
#define _IVTENTRY_H_

#include "KernelEv.h"
#include "lock.h"
#include "event.h"
#include "TimerK.h"

typedef unsigned char IVTNo;
class KernelEv;

class IVTEntry
{
public:

	IVTEntry(IVTNo ivtNo, InterruptRoutine newRoutine);
	~IVTEntry();

	IVTNo ivtNo;
	InterruptRoutine oldRoutine;
	void signalEvent();
	static IVTEntry *entries[256];
	KernelEv *events;

};


#endif
