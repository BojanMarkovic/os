#ifndef _SEMAPHOR_H_ 
#define _SEMAPHOR_H_

#include "krnlsem.h"

class KernelSem;

class Semaphore {
public:
	Semaphore(int init = 1);
	~Semaphore();

	int wait(int toBlock);
	void signal();
	int val() const{
		return myImpl->val();
	} // Returns the current value of the semaphore
private:
	KernelSem* myImpl;
};

#endif
