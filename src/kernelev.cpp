#include "kernelev.h" 

KernelEv::KernelEv(IVTNo n) {
	lock
	ivtEntry = n;
	waitList = new List();
	value=0;
	owner=(PCB*)Start::running;
	IVTEntry::entries[ivtEntry]->events=this;
	unlock
}

KernelEv::~KernelEv() {
	lock
	IVTEntry::entries[ivtEntry]=0;
	delete waitList;
	waitList=0;
	unlock

}

void KernelEv::wait() {
	if(owner==(PCB*)Start::running){
		value--;
		if (value < 0) {
			Start::running->status = PCB::BLOCKED;
			waitList->add((PCB*) Start::running);
			PCB::dispatch();
		}
	}
}

void KernelEv::signal() {
	if(value<1){
		value++;
		if (value <= 0) {
			waitList->setIterHead();
			while(waitList->getIter()!=0){
				PCB*temp=waitList->getIter();
				temp->status=PCB::READY;
				waitList->remove(temp);
				Scheduler::put(temp);
			}
			PCB::dispatch();
		}
	}
}
