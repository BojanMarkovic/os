#include "LIST.h"


List::List() {
	head = last = iter = 0;
	flag = 0;
	br = 0;
}

List::~List() {
	Elem* temp;
	while (head != 0) {
		temp = head;
		head = head->next;
		delete temp;
	}
	delete head;
	delete last;
	delete iter;
	head = last = iter = 0;
}

PCB* List::get() {
	if (head == 0)
		return 0;
	br--;
	Elem* temp = head;
	head = head->next;
	return temp->data;
}

void List::add(PCB* dat) {
	Elem *temp=new Elem(dat);
	if(head==0){
		head=temp;
	}else{
		last->next=temp;
	}
	last=temp;
	br++;
}

void List::goNextIter() {
	if (flag != 1) {
		iter = iter->next;
	}
	flag = 0;
}


int List::remove(PCB*p) {
	Elem *temp = head, *before = 0;
	while ((temp != 0) && (temp->data != p)) {
		before = temp;
		temp = temp->next;
	}
	if (temp != 0) {
		if (before == 0) {
			head = head->next;
			setIterHead();
			flag = 1;
		} else {
			before->next = temp->next;
			iter = before;
			flag = 0;
		}
		if (temp == last)
			last = before;
		br--;
		delete temp;
		return 1;
	}
	return 0;
}
