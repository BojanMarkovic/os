#include "pcb.h" 

PCB::PCB(Thread* myThread, StackSize stackSize, Time timeSlice) {
	lock
	status = PCB::NEW;
	this->timeSlice = timeSlice;
	this->stackSize = stackSize;
	this->myThread = myThread;
	leftTime = timeSlice;
	stack = 0;
	waitList = new List();
	unlock
}
PCB::~PCB() {
	lock
	delete waitList;
	delete stack;
	waitList=0;
	stack=0;
	unlock
}

void PCB::startPCB() {
	status = PCB::READY;
	createStack();
	Scheduler::put(this);
}

void PCB::waitToCompletePCB() {
	if ((status == PCB::OVER)||(this == (PCB*) Start::running)||(myThread == Start::starting)||(myThread == Start::idle))
		return;
	Start::running->status = PCB::BLOCKED;
	waitList->add((PCB*) Start::running);
	dispatch();
}

void PCB::createStack() {
	static volatile unsigned tsp, tss, tip, tcs, oldss, oldsp;
	static unsigned long *tempStack;
	tempStack = new unsigned long[this->stackSize];
	this->stack = tempStack;
	tsp = this->sp = FP_OFF(tempStack + this->stackSize);
	tss = this->ss = FP_SEG(tempStack + this->stackSize);
	tip = FP_OFF(&(wrapper));
	tcs = FP_SEG(&(wrapper));

	asm {
		mov oldss, ss
		mov oldsp, sp
		mov ss, tss
		mov sp, tsp

		pushf
		pop ax
		or ax, 200h
		push ax

		mov ax, tcs
		push ax
		mov ax, tip
		push ax

		mov ax, 0
		push ax
		push bx
		push cx
		push dx
		push es
		push ds
		push si
		push di
		push bp

		mov tsp, sp
		mov tss, ss
		mov sp, oldsp
		mov ss, oldss
	}
	this->sp = tsp;
	this->ss = tss;
}

void PCB::clearWaitingList(){
	waitList->setIterHead();
	while(waitList->getIter()!=0){
		PCB*temp=waitList->getIter();
		temp->status=PCB::READY;
		waitList->remove(temp);
		Scheduler::put(temp);

	}
}

void PCB::wrapper(){
	Start::running->myThread->run();
	lock
	Start::running->status=PCB::OVER;
	Start::running->clearWaitingList();
	unlock
	dispatch();
}

void PCB::dispatch(){
	Start::dispatched = 1;
	TimerK::TIMER();
}

void PCB::clearSleepList(){
	PCB*temp;
	Start::sleepList->setIterHead();
	while(Start::sleepList->getIter()!=0){
		temp=Start::sleepList->getIter();
		if((--temp->leftTime)==0){
			temp->status=READY;
			Start::sleepList->remove(temp);
			Scheduler::put(temp);
		}
		Start::sleepList->goNextIter();
	}
}

void PCB::sleepPCB(Time timeToSleep){
	Start::running->leftTime=timeToSleep;
	Start::running->status=PCB::SLEEPING;
	Start::sleepList->add((PCB*)Start::running);
	Start::sleepFlag=1;
	dispatch();
}
