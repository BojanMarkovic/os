#include "event.h" 
#include "kernelev.h"
#include "lock.h"
#include "IVTEntry.h"

Event::Event(IVTNo n) {
	lock
	myImpl = new KernelEv(n);
	unlock
}

Event::~Event() {
	lock
	delete myImpl;
	unlock
}

void Event::wait() {
	lock
	myImpl->wait();
	unlock
}

void Event::signal() {
	lock
	myImpl->signal();
	unlock
}
