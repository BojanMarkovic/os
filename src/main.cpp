#include "Start.h"
#include "user.h"

extern int userMain(int argc, char* argv[]);

int main(int argc, char* argv[]) {
	Start::first();
	int value = userMain(argc, argv);
	Start::last();
	return value;
}
