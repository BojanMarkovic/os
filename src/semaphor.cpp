#include "semaphor.h" 

Semaphore::Semaphore(int init) {
	lock
	myImpl = new KernelSem(init);
	unlock
}

Semaphore::~Semaphore() {
	lock
	delete myImpl;
	myImpl=0;
	unlock
}
int Semaphore::wait(int toBlock) {
	return myImpl->wait(toBlock);
}

void Semaphore::signal() {
	lock
	myImpl->signal();
	unlock
}
