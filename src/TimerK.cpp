#include "TimerK.h"

void interrupt (*TimerK::oldRoutine)() = 0;
extern void tick();

void interrupt TimerK::TIMER() {
	static volatile unsigned int tsp, tss;
	static volatile PCB *newThread;
	if (!Start::dispatched) {
		tick();
		(*oldRoutine)();
		PCB::clearSleepList();

		if (Start::running->timeSlice != 0)
			Start::running->leftTime -= 1;
		if(Start::running->timeSlice==0)return;
		if(Start::running->leftTime !=0)return;
	}
	if(Start::sleepFlag){
		Start::sleepFlag=0;
	}else if (Start::running->status == PCB::READY && Start::running != Start::idle->myPCB)
		Scheduler::put((PCB*) Start::running);
	while ((Start::running==Start::idle->myPCB)||Start::dispatched) {
		newThread = Scheduler::get();
		if (newThread == 0)
			newThread = Start::idle->myPCB;

		if (newThread->status != PCB::READY)
			continue;

		asm {
			 mov tsp, sp
			 mov tss, ss
		 }
		Start::running->sp = tsp;
		Start::running->ss = tss;
		Start::running = newThread;
		tsp = Start::running->sp;
		tss = Start::running->ss;
		asm {
			 mov sp, tsp
			 mov ss, tss
		}
		Start::running->leftTime =  Start::running->timeSlice;
		if (Start::dispatched)
			Start::dispatched = 0;
		break;
		}
	return;
}

InterruptRoutine TimerK::replaceRoutine(IVTNo intN,
		InterruptRoutine newRoutine) {
	lock
	unsigned int SEGM = FP_SEG(newRoutine);
	unsigned int OFFS = FP_OFF(newRoutine);

	unsigned int OLDSEG, OLDOFF;
	InterruptRoutine old;

	unsigned int k=(unsigned int)intN;
	k *= 4;

	asm cli
	 asm {
		 push es
		 push ax
		 push bx

		 mov ax,0
		 mov es,ax
		 mov bx, word ptr k

		 mov ax, word ptr es:bx+2
		 mov word ptr OLDSEG, ax
		 mov ax, word ptr es:bx
		 mov word ptr OLDOFF, ax

		 mov ax, word ptr SEGM
		 mov word ptr es:bx+2, ax
		 mov ax, word ptr OFFS
		 mov word ptr es:bx, ax

		 pop bx
		 pop ax
		 pop es
	}
	old = (InterruptRoutine) MK_FP(OLDSEG,OLDOFF);
	unlock
	return old;
}
