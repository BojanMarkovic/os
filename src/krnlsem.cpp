#include "krnlsem.h" 

KernelSem::KernelSem(int init) {
	value = init;
}

int KernelSem::wait(int toBlock) {
	lock
	if ((toBlock!=0)||((value >0)&&(toBlock==0))) {
		value--;
		if (value < 0) {
			Start::running->status = PCB::BLOCKED;
			waitQueue.add((PCB*) Start::running);
			PCB::dispatch();
			unlock
			return 1;
		}
	}else{
		unlock
		return -1;
	}
	unlock
	return 0;
}

void KernelSem::signal() {
	value++;
	if (value <= 0) {
		PCB* t = waitQueue.get();
		t->status = PCB::READY;
		Scheduler::put(t);
	}
}
