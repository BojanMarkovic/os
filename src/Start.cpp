#include "Start.h"

volatile int Start::dispatched = 0;
volatile int Start::sleepFlag = 0;
volatile PCB* Start::running = 0;
Thread* Start::starting = 0;
IdleThread* Start::idle = 0;
volatile List *Start::sleepList=0;

void Start::first() {
	lock
	TimerK::oldRoutine = TimerK::replaceRoutine(8, TimerK::TIMER);

	idle = new IdleThread();
	idle->start();
	sleepList=new List();
	
	starting = new Thread(256, 1);
	starting->myPCB->status = PCB::READY;
	running = (volatile PCB*) starting->myPCB;
	unlock
}
void Start::last(){
	lock
	if ((PCB*) running != starting->myPCB){
		unlock
		return;
	}
	TimerK::replaceRoutine(8, TimerK::oldRoutine);
	delete starting;
	delete running;
	delete idle;
	delete sleepList;
	unlock
}


