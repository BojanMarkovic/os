#include "thread.h" 
#include "pcb.h"

Thread::Thread(StackSize stackSize, Time timeSlice) {
	lock
	myPCB = new PCB(this, stackSize, timeSlice);
	unlock
}

Thread::~Thread() {
	waitToComplete();
	lock
	delete myPCB;
	myPCB=0;
	unlock
}

void Thread::start() {
	lock
	myPCB->startPCB();
	unlock
}
 
void Thread::waitToComplete() {
	lock
	myPCB->waitToCompletePCB();
	unlock
}

void dispatch() {
	lock
	PCB::dispatch();
	unlock
}

void Thread::sleep(Time timeToSleep){
	lock
	PCB::sleepPCB(timeToSleep);
	unlock
}
