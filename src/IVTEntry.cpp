#include "IVTEntry.h"

IVTEntry* IVTEntry::entries[256];

IVTEntry::IVTEntry(IVTNo ivtNo, InterruptRoutine newRoutine) {
	lock
	this->ivtNo = ivtNo;
	oldRoutine = TimerK::replaceRoutine(ivtNo, newRoutine);
	events = 0;
	IVTEntry::entries[ivtNo] = this;
	unlock
}

IVTEntry::~IVTEntry() {
	lock
	if(oldRoutine){
		oldRoutine();
		TimerK::replaceRoutine(ivtNo, oldRoutine);
	}
	delete events;
	events =0;
	IVTEntry::entries[ivtNo] = 0;
	unlock
}

void IVTEntry::signalEvent() {
	lock
	events->signal();
	unlock

}
